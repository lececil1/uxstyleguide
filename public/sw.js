'use strict';

const cacheVersion = 1606166703;
const offlineCache = 'offline-' + cacheVersion;
const offlinePage = '/offline/';
const debugMode = false;

const filesToCache = [
  "/",
  
    "/404.html",
  
    "/src/patterns/foundations/color/accent-colors.html",
  
    "/src/patterns/components/button/animated_button.html",
  
    "/src/patterns/components/button/block_button.html",
  
    "/src/patterns/components/blockquote/blockquote.html",
  
    "/src/patterns/components/button/default_button.html",
  
    "/src/patterns/components/button/disabled_button.html",
  
    "/src/patterns/templates/full-width/full-width.html",
  
    "/src/patterns/components/heading/headline-1-3.html",
  
    "/src/patterns/components/heading/headline-4.html",
  
    "/src/patterns/foundations/branding/heros.html",
  
    "/src/patterns/foundations/branding/icons.html",
  
    "/",
  
    "/src/patterns/components/label/label.html",
  
    "/src/patterns/components/list/ordered.html",
  
    "/src/patterns/foundations/color/primary-colors.html",
  
    "/src/patterns/foundations/typography/roboto-slab.html",
  
    "/src/patterns/foundations/typography/roboto.html",
  
    "/src/patterns/components/heading/subtitle.html",
  
    "/src/patterns/components/list/unordered.html",
  
    
  
    
 
      "/LICENSE",
  
      "/README.md",
  
      "/manifest.json",
  
      "/src/assets/fonts/roboto-slab/RobotoSlab-Bold.ttf",
  
      "/src/assets/fonts/roboto-slab/RobotoSlab-Light.ttf",
  
      "/src/assets/fonts/roboto-slab/RobotoSlab-Regular.ttf",
  
      "/src/assets/fonts/roboto/Roboto-Bold.ttf",
  
      "/src/assets/fonts/roboto/Roboto-BoldItalic.ttf",
  
      "/src/assets/fonts/roboto/Roboto-Italic.ttf",
  
      "/src/assets/fonts/roboto/Roboto-Light.ttf",
  
      "/src/assets/fonts/roboto/Roboto-LightItalic.ttf",
  
      "/src/assets/fonts/roboto/Roboto-Regular.ttf",
  
      "/src/assets/images/icon-1024x1024.png",
  
      "/src/assets/images/icon-128x128.png",
  
      "/src/assets/images/icon-16x16.png",
  
      "/src/assets/images/icon-256x256.png",
  
      "/src/assets/images/icon-32x32.png",
  
      "/src/assets/images/icon-512x512.png",
  
      "/src/assets/images/icon-64x64.png",
  
      "/src/assets/images/icon.ico",
  
      "/src/assets/images/icon.svg",
  
      "/src/assets/js/libs/jquery-2.1.4.min.js",
  
      "/styleguide/branding/favicon.ico",
  
      "/styleguide/branding/png/styleguide-1024x1024.png",
  
      "/styleguide/branding/png/styleguide-128x128.png",
  
      "/styleguide/branding/png/styleguide-16x16.png",
  
      "/styleguide/branding/png/styleguide-256x256.png",
  
      "/styleguide/branding/png/styleguide-32x32.png",
  
      "/styleguide/branding/png/styleguide-512x512.png",
  
      "/styleguide/branding/png/styleguide-64x64.png",
  
      "/styleguide/branding/styleguide.svg",
  
      "/styleguide/css/styleguide.css",
  
      "/styleguide/css/styleguide.scss",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.eot",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.otf",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.svg",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.ttf",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.woff",
  
      "/styleguide/fonts/HKGrotesk/Bold/HKGrotesk-Bold.woff2",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.eot",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.otf",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.svg",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.ttf",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.woff",
  
      "/styleguide/fonts/HKGrotesk/BoldItalic/HKGrotesk-BoldItalic.woff2",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.eot",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.otf",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.svg",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.ttf",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.woff",
  
      "/styleguide/fonts/HKGrotesk/Italic/HKGrotesk-Italic.woff2",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.eot",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.otf",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.svg",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.ttf",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.woff",
  
      "/styleguide/fonts/HKGrotesk/Light/HKGrotesk-Light.woff2",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.eot",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.otf",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.svg",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.ttf",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.woff",
  
      "/styleguide/fonts/HKGrotesk/LightItalic/HKGrotesk-LightItalic.woff2",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.eot",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.otf",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.svg",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.ttf",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.woff",
  
      "/styleguide/fonts/HKGrotesk/Medium/HKGrotesk-Medium.woff2",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.eot",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.otf",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.svg",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.ttf",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.woff",
  
      "/styleguide/fonts/HKGrotesk/MediumItalic/HKGrotesk-MediumItalic.woff2",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.eot",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.otf",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.svg",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.ttf",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.woff",
  
      "/styleguide/fonts/HKGrotesk/Regular/HKGrotesk-Regular.woff2",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-SemiBold.otf",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-Semibold.eot",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-Semibold.svg",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-Semibold.ttf",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-Semibold.woff",
  
      "/styleguide/fonts/HKGrotesk/Semibold/HKGrotesk-Semibold.woff2",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiBoldItalic.otf",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiboldItalic.eot",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiboldItalic.svg",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiboldItalic.ttf",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiboldItalic.woff",
  
      "/styleguide/fonts/HKGrotesk/SemiboldItalic/HKGrotesk-SemiboldItalic.woff2",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.eot",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.ijmap",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.svg",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.ttf",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.woff",
  
      "/styleguide/fonts/MaterialIcons/MaterialIcons-Regular.woff2",
  
      "/styleguide/fonts/MaterialIcons/codepoints",
  
      "/styleguide/images/cover.png",
  
      "/styleguide/images/flexible.png",
  
      "/styleguide/images/html.png",
  
      "/styleguide/images/loader.svg",
  
      "/styleguide/images/source/styleguide_source.ai",
  
      "/styleguide/js/libs/iframeResizer.contentWindow.min.js",
  
      "/styleguide/js/libs/iframeResizer.min.js",
  
      "/styleguide/js/libs/jquery-2.1.4.min.js",
  
      "/styleguide/js/libs/list.min.js",
  
      "/styleguide/js/main.js",
  
      "/styleguide/js/resize.js",
  
    
  "css/main.css",
  "/about/",
  "/index.html",
  "/offline/"
];

/**
 * Delete caches that do not match the current version of the service worker.
 *
 * @returns {*|Promise.<TResult>}
 */
function clearOldCaches() {
    debug("Clean old caches");
    return caches.keys().then(keys => {
        return Promise.all(
            keys
                .filter(key => key.indexOf(offlineCache) !== 0)
                .map(key => caches.delete(key))
        );
    });
}

function cacheOfflinePage() {
    debug("Cache offline page");
    return caches.open(offlineCache)
        .then(cache => {
            return cache.addAll([offlinePage]);
        })
        .catch(error => {
            debug(error);
        })
}

/**
 * Install Service Worker
 */
 self.addEventListener('install', event => {
    debug("Installing Service Worker");
    event.waitUntil(
        cacheOfflinePage()
        .then( () => self.skipWaiting() )
    );
});

/**
 * Activate Service Worker
 */
self.addEventListener('activate', event => {
    debug("Activating Service Worker");
    event.waitUntil(
        clearOldCaches()
            .then(() => self.clients.claim())
    );
});

self.addEventListener('fetch', event => {
    let request = event.request;

    // Ignore non-GET requests
    if (request.method !== 'GET') {
        debug("Ignoring non GET request");
        return;
    }

    event.respondWith(
        caches.match(request)
            .then(response => {
                // CACHE
                debug("Fetching " + request.url)

                if (response) {
                    debug("Found in cache: " + request.url);
                }

                return response || fetch(request)
                    .then( response => {
                        // NETWORK
                        debug("Going to network: " + request.url);
                        if (response && response.ok) {
                            debug("Saving in cache: " + request.url);
                            let copy = response.clone();
                            caches.open(offlineCache)
                                .then( cache => cache.put(request, copy) );
                        }
                        return response;
                    })
                    .catch( error => {
                        // OFFLINE
                        debug("Offline and no cache for: " + request.url + ": " + error);
                        if (request.mode == 'navigate') {
                            debug("Showing offline page")
                            return caches.match(offlinePage);
                        } else if (request.headers.get('Accept').indexOf('image') !== -1) {
                            return new Response('<svg role="img" aria-labelledby="offline-title" viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg"><title id="offline-title">Offline</title><g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/><text fill="#9B9B9B" font-family="Helvetica Neue,Arial,Helvetica,sans-serif" font-size="72" font-weight="bold"><tspan x="93" y="172">offline</tspan></text></g></svg>', {headers: {'Content-Type': 'image/svg+xml'}});
                        }
                    });
            })
      );
});

function debug(message) {
    if (debugMode) {
        console.log(message);
    }
}